﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Petrify : MonoBehaviour
{
    public GameObject playerSoul;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playerSoul.transform.position;
        transform.rotation = playerSoul.transform.rotation;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Enemy") && collision.gameObject.layer != 17)
        {
            Debug.Log("PETRIFY");
            EnemyPatrol enemyPatrol = collision.transform.GetComponent<EnemyPatrol>();
            Instantiate(enemyPatrol.enemyPetrify, new Vector3(collision.transform.position.x, collision.transform.position.y, collision.transform.position.z), collision.transform.rotation);
            Destroy(enemyPatrol.enemyObject);
        }
    }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Enemy" && collision.gameObject.layer != 17)
            {
                Debug.Log("PETRIFY");
                EnemyPatrol enemyPatrol = collision.transform.GetComponent<EnemyPatrol>();
                Instantiate(enemyPatrol.enemyPetrify, new Vector3 (collision.transform.position.x, collision.transform.position.y, collision.transform.position.z), collision.transform.rotation);
                Destroy(enemyPatrol.enemyObject);
            }
        }
}
