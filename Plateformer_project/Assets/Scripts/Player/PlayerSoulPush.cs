﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoulPush : MonoBehaviour
{
    public bool push;
    public bool action;
    public bool contact;

    public Player player;

    public static PlayerSoulPush instance;



    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerSoulPush dans la scène");
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(1))
        {
            action = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            action = false;
            push = false;
        }

        if ((contact) && (action))
        {
            push = true;
        }
        else
        {
            push = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Crate"))
        {
            player.controller.isMoving = true;
            SoulCrate crate = collision.transform.GetComponent<SoulCrate>();
            crate.contact = true;
            contact = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Crate"))
        {
            SoulCrate crate = collision.transform.GetComponent<SoulCrate>();
            crate.contact = false;
            contact = false;
            player.controller.isMoving = false;
        }
    }

    /*void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Crate")
        {
            player.controller.isMoving = true;
            SoulCrate crate = other.transform.GetComponent<SoulCrate>();
            crate.contact = true;
            contact = true;
        }
    }*/

    /*void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Crate")
        {
            SoulCrate crate = other.transform.GetComponent<SoulCrate>();
            crate.contact = false;
            contact = false;
            player.controller.isMoving = false;
        }
    }*/
}
