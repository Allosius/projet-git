﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoul : MonoBehaviour
{
    //public BoxCollider2D soulCollider;
    //public float maxSoulDuration = 200f;
    //public float currentSoulDuration;

    //public ChargBar chargBar;

    public int spearStock = 0;
    public Vector3 rotationStaticSpear;

    public bool arrow = false;
    public bool spear = false;

    public Player player;

    public float ejectTimeDuration;

    public static PlayerSoul instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerSoul dans la scène");
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

        //chargBar.SetMaxDurationSoulBar(maxSoulDuration);

        //Physics2D.IgnoreCollision(soulCollider, GameObject.FindGameObjectWithTag("Enemy").GetComponent<Collider2D>());
        Physics2D.IgnoreLayerCollision(14, 15);         // ID 14 = layer PlayerSoul, ID 15 = layer PhysicObjects
    }

    // Update is called once per frame
    void Update()
    {
        if(spearStock > 1)
        {
            spearStock = 1;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (spearStock >= 1)
            {
                player.isAttacking = true;
                StartCoroutine(projectileEject());
                
            }
        }
        

        /*if (PlayerManager.instance.soul)
        {
            currentSoulDuration -= 0.1f;
            chargBar.SetDurationSoulBar(currentSoulDuration);
        }

        if(currentSoulDuration <= 0f)
        {
            GameManager.instance.OnPlayerDeath();
        }*/
    }

    public IEnumerator projectileEject()
    {
        yield return new WaitForSeconds(ejectTimeDuration);

        Instantiate(PlayerManager.instance.soulProjectileReturn, transform.position, transform.rotation);
        SpriteRenderer spriteRenderer = PlayerManager.instance.staticSpear.GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        PlayerManager.instance.staticSpear.GetComponent<StaticProjectile>().enabled = false;
        spearStock = 0;

        player.isAttacking = false;
    }
}
