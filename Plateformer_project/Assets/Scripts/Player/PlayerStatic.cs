﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatic : MonoBehaviour
{
    public Rigidbody2D rb;
    public Collider2D staticCollider;

    private bool isGrounded;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask collisionLayers;

    public Transform transformPlayerBody;

    public Transform transformPlayerStatic;

    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(0, 17);         // ID 0 = layer Default, ID 17 = layer SoulObjects
        Physics2D.IgnoreLayerCollision(0, 14);         // ID 0 = layer Default, ID 14 = layer PlayerSoul
    }

    // Update is called once per frame
    void Update()
    {
        if(isGrounded)
        {
            transformPlayerBody.position = transformPlayerStatic.position;
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
            staticCollider.enabled = false;
        }
        else if (!isGrounded)
        {
            staticCollider.enabled = true;
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, collisionLayers);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
