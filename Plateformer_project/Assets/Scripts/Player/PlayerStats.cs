﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int maxHealth = 3;
    public int currentHealth;

    public float deathAnimationTime = 1.0f;
    public float effectAnimationTime = 0.5f;
    public GameObject smokeEffect;

    public float damageFlashDelay = 0.2f;

    public SpriteRenderer graphics;

    public HealthBar healthBar;

    public static PlayerStats instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerStats dans la scène");
            return;
        }

        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        //healthBar.SetMaxHealth(maxHealth);


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*public void HealPlayer(int amount)
    {
        if ((currentHealth + amount) > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else
        {
            currentHealth += amount;
        }
        //healthBar.SetHealth(currentHealth);
    }*/

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        StartCoroutine(EffectDamage());
        //healthBar.SetHealth(currentHealth);

        // vérifier si le joueur est toujours vivant
        if (currentHealth <= 0)
        {
            Death();
            return;
        }

       /*if(currentHealth == 2)
        {
            healthBar.health03.SetActive(false);
        }
        else if (currentHealth == 1)
        {
            healthBar.health03.SetActive(false);
            healthBar.health02.SetActive(false);
        }*/

    }

    public void ResurectPlayer()
    {
        currentHealth = maxHealth;
    }

    public IEnumerator EffectDamage()
    {
        //graphics.color = new Color(1f, 0f, 0f, 0.5f);
        yield return new WaitForSeconds(damageFlashDelay);
        //graphics.color = new Color(1f, 1f, 1f, 1f);
        //yield return new WaitForSeconds(1f);
    }

    public void Death()
    {
        Debug.Log("le joueur est mort");
        //healthBar.health03.SetActive(false);
        //healthBar.health02.SetActive(false);
        //healthBar.health01.SetActive(false);

        StartCoroutine(PlayerManager.instance.PlayerDeath());
    }

    /*public IEnumerator EffectDeath()
    {
        

        //yield return new WaitForSeconds(deathAnimationTime);
        //graphics.enabled = false;
        //Instantiate(smokeEffect, transform.position, transform.rotation);
        yield return new WaitForSeconds(effectAnimationTime);

        PlayerManager.instance.SoulForm();
        PlayerManager.instance.death = true;

        //GameManager.instance.OnPlayerDeath();
    }*/
}
