﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileReturn : MonoBehaviour
{
    private Vector3 RotateChange;
    private Vector2 mousePosition;

    public bool isDragging = true;

    public float speed = 5f;

    //public CursorMode cursorMode = CursorMode.Auto;
    //public Vector2 hotSpot = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.SetCursor(null, hotSpot, cursorMode);
        RotateChange = new Vector3(0f, 180f, 0f);
        transform.Rotate(RotateChange);
        PlayerAttack.instance.strike = false;
        Debug.Log("return projectile created");
    }

    // Update is called once per frame
    void Update()
    {
        if (isDragging)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            isDragging = false;
        }
        transform.Translate(mousePosition.normalized * speed * Time.deltaTime, Space.World);
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if ((hitInfo.CompareTag("DeathZone")) || (hitInfo.CompareTag("Enemy")) || (hitInfo.CompareTag("Ground")))
        {
            //Instantiate(impactEffect, transform.position, transform.rotation);
            //AudioManager.instance.PlaySFX(3);

            Destroy(gameObject);
        }

        if (hitInfo.CompareTag("Lever") && hitInfo.gameObject.layer == 15)
        {
            Lever_Spawn_Blocks leverSpawnBlocks = hitInfo.transform.GetComponent<Lever_Spawn_Blocks>();
            leverSpawnBlocks.input = true;

            Debug.Log("projectile_Lever");

            Destroy(gameObject);
        }
    }
}
