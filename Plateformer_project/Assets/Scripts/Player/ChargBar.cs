﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargBar : MonoBehaviour
{
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMaxDurationSoulBar(float duration)
    {
        slider.maxValue = duration;
        slider.value = duration;

    }

    public void SetDurationSoulBar(float duration)
    {
        slider.value = duration;

    }
}
