﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoulReturn : MonoBehaviour
{
    public float speed = 2.5f;

    public Transform player;
    public Rigidbody2D rb;

    public GameObject playerSoulAlpha;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;


    }

    // Update is called once per frame
    void Update()
    {
        Instantiate(playerSoulAlpha, transform.position, transform.rotation);


        Vector2 target = new Vector2(player.position.x, player.position.y);
        Vector2 newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);

        rb.MovePosition(newPos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Soul Trigger Player");
            Destroy(gameObject);
        }
    }
}
