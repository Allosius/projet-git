﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class Player : MonoBehaviour
{

	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 6;

	float gravity;
	float jumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;

	private bool isGrounded;
	public bool isPushing;
	public bool isAttacking;
	public Transform groundCheck;
	public float groundCheckRadius;

	private Vector3 RotateChange;
	public Transform graphics;

	public Animator animator;

	public bool enemyCollision;

	public Rigidbody2D rb;

	public PlayerController controller;

	void Start()
	{
		controller = GetComponent<PlayerController>();

		gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		print("Gravity: " + gravity + "  Jump Velocity: " + jumpVelocity);
	}

	/*private void OnTriggerEnter2D(Collider2D collision)
	{
		if ((collision.CompareTag("Weakspot")) || (collision.CompareTag("Enemy")))
		{
			
		}
	}*/

	void Update()
	{
		float characterVelocity = Mathf.Abs(velocity.x);
		//Debug.Log(characterVelocity);

		if (characterVelocity < 0.3f)
		{
			controller.isMoving = false;
		}
		else if (characterVelocity >= 0.3f)
        {
			controller.isMoving = true;
		}

		if (enemyCollision)
        {
			velocity.y = jumpVelocity;
			velocity.y += gravity * Time.deltaTime;
			controller.Move(velocity * Time.deltaTime);
			Debug.Log("jump Enemy");
			enemyCollision = false;
		}

		Flip(velocity.x);

		
		

		if (!isPushing)
		{
			animator.SetFloat("Speed", characterVelocity);
		}
		animator.SetBool("isJumping", !isGrounded);
		animator.SetBool("isPushing", isPushing);
		animator.SetBool("isAttacking", isAttacking);


		if (controller.collisions.above || controller.collisions.below)
		{
			velocity.y = 0;
		}

		Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		//Debug.Log(input);

		if (Input.GetKeyDown(KeyCode.Space) && controller.collisions.below)
		{
			velocity.y = jumpVelocity;
		}

		float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
		//Debug.Log(velocity.x);
		velocity.y += gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime);
	}

	

	void FixedUpdate()
    {
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, controller.collisionMask);
		//Debug.Log("isGrounded :" + isGrounded);

		if(isGrounded)
        {
			PlayerAttack.instance.canAttacking = true;
        }
		else
        {
			PlayerAttack.instance.canAttacking = false;
        }
    }

	void Flip(float _velocity)
	{
		if((Input.GetKeyDown(KeyCode.LeftArrow)) || (Input.GetKeyDown(KeyCode.Q)))
        {
			RotateChange = new Vector3(0f, 180f, 0f);
			CameraFollow.instance.posOffset.x = CameraFollow.instance.xneg;
			PlayerManager.instance.playerIsFlipping = true;
			graphics.transform.rotation = Quaternion.Euler(RotateChange);
			if(PlayerManager.instance.soul)
            {
				PlayerManager.instance.soulFlipping = true;
            }
			//graphics.transform.Rotate(RotateChange);
		}

		if ((Input.GetKeyDown(KeyCode.RightArrow)) || (Input.GetKeyDown(KeyCode.D)))
        {
			RotateChange = new Vector3(0f, 0f, 0f);
			CameraFollow.instance.posOffset.x = CameraFollow.instance.xpos;
			PlayerManager.instance.playerIsFlipping = false;
			graphics.transform.rotation = Quaternion.Euler(RotateChange);
			if (PlayerManager.instance.soul)
			{
				PlayerManager.instance.soulFlipping = false;
			}
			//graphics.transform.Rotate(RotateChange);
		}
		//Debug.Log(_velocity);
	}

	public IEnumerator Knockback(float knockDur, float knockbackPwr, Vector3 knockbackDir)
    {
		float timer = 0;

		while(knockDur > timer)
        {
			timer += Time.deltaTime;

			rb.AddForce(new Vector3(knockbackDir.x * -100, knockbackDir.y * knockbackPwr, transform.position.z));
        }

		yield return 0;
    }

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
	}
}
