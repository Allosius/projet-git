﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public GameObject health01, health02, health03;

    // Start is called before the first frame update
    void Start()
    {
        health01.SetActive(true);
        health02.SetActive(true);
        health03.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
