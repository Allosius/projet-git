﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulProjectileReturn : MonoBehaviour
{
    private Vector3 RotateChange;

    public Rigidbody2D rb;

    public float speed = 5f;

    public GameObject[] staticProjectilePlanted;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(17, 15);

        staticProjectilePlanted = GameObject.FindGameObjectsWithTag("StaticSpear");
        RotateChange = new Vector3(0f, 180f, 0f);
        if (PlayerManager.instance.soulFlipping)
        {
            transform.Rotate(RotateChange);
            PlayerManager.instance.soulFlipping = false;
        }
        
        Debug.Log("soul return projectile created");

        rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        /*for (int i = 0; i < staticProjectilePlanted.Length; i++)
        {
            Destroy(staticProjectilePlanted[i]);
        }*/
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if ((hitInfo.CompareTag("DeathZone")) || (hitInfo.CompareTag("Enemy")) || (hitInfo.CompareTag("Ground")))
        {
            //Instantiate(impactEffect, transform.position, transform.rotation);
            //AudioManager.instance.PlaySFX(3);

            Destroy(gameObject);
        }

        if (hitInfo.CompareTag("Lever") && hitInfo.gameObject.layer == 17)
        {
            SoulLeverSpawnBlocks soulLeverSpawnBlocks = hitInfo.transform.GetComponent<SoulLeverSpawnBlocks>();
            soulLeverSpawnBlocks.input = true;

            Debug.Log("projectile_Lever");

            Destroy(gameObject);
        }
    }
}
