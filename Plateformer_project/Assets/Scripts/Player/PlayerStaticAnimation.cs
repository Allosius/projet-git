﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStaticAnimation : MonoBehaviour
{
    public Animator animatorPlayerStatic;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
         if ((collision.CompareTag("Ground")) || (collision.CompareTag("Crate")) || (collision.CompareTag("Platform")))
         {
            animatorPlayerStatic.Play("Static_GroundFall");
         }

        /*else if(collision.gameObject.layer == 15)
        {
            animatorPlayerStatic.Play("Static_GroundFall");
        }*/
    }
}
