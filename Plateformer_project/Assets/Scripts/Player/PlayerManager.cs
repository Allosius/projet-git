﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public bool death;
    public float deathAnimationDuration;

    public bool restrictedArea;

    public bool gridDeath;

    public bool soul;
    public bool notControllingPlateform = true;

    public bool activeBody;
    public bool activeSoul;

    public bool playerIsFlipping;

    public GameObject playerBody;
    public Transform transformPlayerBody;
    public Animator animatorPlayerBody;

    public GameObject playerStatic;
    public Transform transformPlayerStatic;
    public Animator animatorPlayerStatic;

    //public PlayerMovement playerBodyMovement;
    public BoxCollider2D colliderPlayerBody;
    public BoxCollider2D colliderPlayerBodyWeakspot;
    public Player playerScriptBody;
    public PlayerController playerControllerScriptBody;

    //public Rigidbody2D rbBody;

    public GameObject soulColliderEnemy;
    public GameObject playerSoul;
    public Transform  transformPlayerSoul;

    public GameObject playerSoulReturn;

    public GameObject soulProjectileReturn;
    public GameObject soulProjectileArrow;
    public GameObject soulProjectileSpear;
    public bool soulFlipping;
    public GameObject staticSpear;

    public Transform respawn;

    public static PlayerManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerManager dans la scène");
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
    }

    public void SoulForm()
    {
        //Desactivation trail d'effet de renvoi de projectiles
        PlayerAttack.instance.strike = false;
        PlayerAttack.instance.trailAnimation.Play("IdleAnimation");
        PlayerAttack.instance.trail.SetActive(false);

        playerBody.SetActive(false);

        //playerBodyMovement.enabled = false;
        playerScriptBody.enabled = false;
        playerControllerScriptBody.enabled = false;
        //rbBody.bodyType = RigidbodyType2D.Kinematic;
        //rbBody.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;


        transformPlayerStatic.position = transformPlayerBody.position;
        playerStatic.SetActive(true);
        animatorPlayerStatic.Play("Static_Idle");

        playerSoul.SetActive(true);
        soulColliderEnemy.SetActive(true);
        activeBody = false;
        activeSoul = true;

        transformPlayerSoul.position = transformPlayerBody.position;

        soul = true;
    }

    public void BodyForm()
    {
        if (PlayerSoul.instance.spearStock >= 1)
        {
            Instantiate(soulProjectileReturn, transformPlayerSoul.position, transformPlayerSoul.rotation);
            SpriteRenderer spriteRenderer = staticSpear.GetComponent<SpriteRenderer>();
            spriteRenderer.enabled = false;
            staticSpear.GetComponent<StaticProjectile>().enabled = false;
            PlayerSoul.instance.spearStock = 0;
        }

        transformPlayerBody.position = transformPlayerStatic.position;
        Instantiate(playerSoulReturn, transformPlayerSoul.position, transformPlayerSoul.rotation);

        soulColliderEnemy.SetActive(false);
        playerSoul.SetActive(false);
        playerStatic.SetActive(false);

        //rbBody.bodyType = RigidbodyType2D.Dynamic;
        //rbBody.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
        //playerBodyMovement.enabled = true;
        playerControllerScriptBody.enabled = true;
        playerScriptBody.enabled = true;

        playerBody.SetActive(true);
        activeBody = true;
        activeSoul = false;

        soul = false;
    }

    public void DeathSoulForm()
    {
        //Desactivation trail d'effet de renvoi de projectiles
        PlayerAttack.instance.strike = false;
        PlayerAttack.instance.trailAnimation.Play("IdleAnimation");
        PlayerAttack.instance.trail.SetActive(false);

        //playerBody.SetActive(false);

        //playerBodyMovement.enabled = false;
        //playerScriptBody.enabled = false;
        //playerControllerScriptBody.enabled = false;
        //rbBody.bodyType = RigidbodyType2D.Kinematic;
        //rbBody.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;

        transformPlayerStatic.position = transformPlayerBody.position;
        //playerStatic.SetActive(true);
        //animatorPlayerStatic.Play("Static_Idle");

        playerSoul.SetActive(true);
        soulColliderEnemy.SetActive(true);
        activeBody = false;
        activeSoul = true;

        transformPlayerSoul.position = transformPlayerBody.position;

        soul = true;
    }

    public IEnumerator PlayerDeath()
    {
        playerScriptBody.enabled = false;
        playerControllerScriptBody.enabled = false;
        colliderPlayerBody.enabled = false;
        colliderPlayerBodyWeakspot.enabled = false;
        animatorPlayerBody.Play("Player_Death");

        yield return new WaitForSeconds(deathAnimationDuration);

        DeathSoulForm();

        death = true;


        //yield return new WaitForSeconds(deathAnimationDuration);

        //transformPlayerBody.position = respawn.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (soul)
        {
            if (PlayerSoul.instance.arrow)
            {
                soulProjectileReturn = soulProjectileArrow;
                staticSpear = GameObject.FindGameObjectWithTag("StaticArrow");
            }

            if (PlayerSoul.instance.spear)
            {
                soulProjectileReturn = soulProjectileSpear;
                staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
            }
        }

        if (!death)
        {
            if (Input.GetButtonDown("Soul"))
            {
                if (!soul && !restrictedArea)
                {
                    SoulForm();
                    
                }
                else if (soul && notControllingPlateform)
                {
                    BodyForm();
                }

            }
        }
    }
}
