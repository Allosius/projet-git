﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoulAlpha : MonoBehaviour
{
    public SpriteRenderer graphics;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        graphics.color -= new Color(0f, 0f, 0f, 0.1f);

        if(graphics.color.a <= 0f)
        {
            Destroy(gameObject);
        }
    }
}
