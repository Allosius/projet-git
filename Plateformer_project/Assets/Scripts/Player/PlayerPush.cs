﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPush : MonoBehaviour
{
    public bool push;
    public bool action;
    public bool contact;

    public Player player;

    public static PlayerPush instance;

    

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerPush dans la scène");
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(1))
        {
            action = true;
        }

        if(Input.GetMouseButtonUp(1))
        {
            action = false;
            push = false;
        }

        if((contact) && (action))
        {
            push = true;
        }
        else
        {
            push = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Crate"))
        {
            player.controller.isMoving = true;
            Crate crate = collision.transform.GetComponent<Crate>();
            crate.contact = true;
            contact = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Crate"))
        {
            Crate crate = collision.transform.GetComponent<Crate>();
            crate.contact = false;
            contact = false;
            player.controller.isMoving = false;
        }
    }
}
