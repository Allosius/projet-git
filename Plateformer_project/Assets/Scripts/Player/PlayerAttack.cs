﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public bool strike = false;

    public float projectileReturnTimeDuration = 1.0f;

    public Player player;

    public static PlayerAttack instance;
    public bool canAttacking;

    public GameObject trail;
    public Animator trailAnimation;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerAttack dans la scène");
            return;
        }

        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canAttacking)
        {
            if ((Input.GetMouseButtonDown(0)) && (strike == false))
            {
                strike = true;
                player.isAttacking = true;
                StartCoroutine(projectileReturn());
            }
        }
    }

    public IEnumerator projectileReturn()
    {
        //trail.SetActive(true);
        //trailAnimation.Play("MagicSwordTrailAnimation");
        yield return new WaitForSeconds(projectileReturnTimeDuration);

        strike = false;
        player.isAttacking = false;
        //trailAnimation.Play("IdleAnimation");
        //trail.SetActive(false);
    }
}
