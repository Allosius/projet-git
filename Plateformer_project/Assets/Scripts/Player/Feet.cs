﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feet : MonoBehaviour
{
    public GameObject player;
    //public GameObject plateform;

    //public RemovablePlatform removablePlatform;

    public PlayerStatic playerStatic;
    public Rigidbody2D rbStatic;
    public Collider2D staticCollider;

    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(0, 17);         // ID 0 = layer Default, ID 17 = layer SoulObjects
        Physics2D.IgnoreLayerCollision(0, 14);         // ID 0 = layer Default, ID 14 = layer PlayerSoul
    }

    // Update is called once per frame
    void Update()
    {
        //player.transform.position = new Vector3(plateform.transform.position.x, plateform.transform.position.y + 1.5f, plateform.transform.position.z);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if((other.gameObject.CompareTag("Platform")) && (gameObject.tag == "PlayerStatic"))
        {
            playerStatic.enabled = false;
            staticCollider.enabled = true;
            rbStatic.bodyType = RigidbodyType2D.Dynamic;
            rbStatic.constraints = RigidbodyConstraints2D.None;
            rbStatic.constraints = RigidbodyConstraints2D.FreezeRotation;

            /*if ((Input.GetButton("Down_Platform")) && (removablePlatform.nextPos == removablePlatform.pos1.position))
            {
                transform.position = Vector3.MoveTowards(transform.position, removablePlatform.nextPos, removablePlatform.speed * 2 * Time.deltaTime);
            }
            else if ((Input.GetButton("Up_Platform")) && (removablePlatform.nextPos == removablePlatform.pos2.position))
            {
                transform.position = Vector3.MoveTowards(transform.position, removablePlatform.nextPos, removablePlatform.speed * 2 * Time.deltaTime);
            }*/
        }
    }

    public void OnCollisionStay(Collision collisionInfo)
    {
        if(collisionInfo.gameObject.tag == "Platform")
        {
            player.transform.parent = collisionInfo.transform;
        }
        else
        {
            transform.parent = null;
        }
    }

    /*public void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Platform"))
        {
            
        }
    }*/
}
