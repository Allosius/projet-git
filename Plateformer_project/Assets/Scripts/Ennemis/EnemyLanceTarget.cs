﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLanceTarget : MonoBehaviour
{
    public Transform playerPoint;
    public float playerY;
    public float targetHigh = 0.8f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        playerY = playerPoint.position.y - targetHigh;
        transform.position = new Vector3(transform.position.x, playerY, transform.position.z);
        //Debug.Log(transform.position);
    }
}
