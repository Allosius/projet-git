﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVisibilityZone : MonoBehaviour
{
    public ShootProjectile shootProjectile;
    public GameObject graphics;

    // Start is called before the first frame update
    void Start()
    {
        shootProjectile.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = graphics.transform.position;
        transform.rotation = graphics.transform.rotation;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!PlayerManager.instance.soul)
        {
            if (graphics.layer == 17)
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootProjectile.enabled = false;
                }
            }
            else
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootProjectile.enabled = true;
                }
            }
        }
        else if (PlayerManager.instance.soul)
        {
            if (graphics.layer == 17)
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootProjectile.enabled = true;
                }
            }
            else
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootProjectile.enabled = false;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
        {
            shootProjectile.enabled = false;
        }
    }
}
