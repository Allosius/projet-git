﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float speed;
    public Transform[] waypoints;

    public int damageOnCollision = 1;

    private Vector3 RotateChange;

    public bool isGrounded;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask collisionMask;

    public float fallSpeed = 0.4f;
    //public Rigidbody2D rb;

    public SpriteRenderer graphics;
    private Transform target;
    private int destPoint;

    public GameObject gobjPlayer;
    public Player player;

    public Animator animator;

    public bool isAttacking;
    public bool isDied;

    public GameObject enemyObject;
    public GameObject enemyPetrify;

    // Start is called before the first frame update
    void Start()
    {
        player = gobjPlayer.GetComponent<Player>();

        target = waypoints[0];

        RotateChange = new Vector3(0f, 180f, 0f);
    }

    void FixedUpdate()
    {
        //isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, collisionMask);
    }

    // Update is called once per frame
    void Update()
    {
        if(!PlayerManager.instance.soul)
        {
            if(gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }
        else if(PlayerManager.instance.soul)
        {
            if (gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0.75f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }


        animator.SetBool("Attack", isAttacking);
        animator.SetBool("Death", isDied);
        animator.SetBool("Grounded", isGrounded);

        if (isGrounded)
        {
            //rb.bodyType = RigidbodyType2D.Static;
            //animator.StartPlayback();

            Vector3 dir = target.position - transform.position;
            transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

            // Si l'ennemi est quasiment arrivé à sa destination
            if (Vector3.Distance(transform.position, target.position) < 0.3f)
            {
                isGrounded = true;
                destPoint = (destPoint + 1) % waypoints.Length;
                target = waypoints[destPoint];
                transform.Rotate(RotateChange);
            }
        }

        if(!isGrounded)
        {
            //animator.StopPlayback();
            //rb.bodyType = RigidbodyType2D.Dynamic;
            //transform.position = transform.position + new Vector3(0f, -fallSpeed, 0f);
            transform.Translate(new Vector3(0f, -fallSpeed * 10, 0f) * Time.deltaTime, Space.World);
        }

        if(isDied)
        {
            speed = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.tag == "PhysicGrid") || (collision.gameObject.tag == "Crate"))
        {
            Debug.Log("EnnemiCollideGrille");
            destPoint = (destPoint + 1) % waypoints.Length;
            target = waypoints[destPoint];
            transform.Rotate(RotateChange);
        }

        if (collision.gameObject.tag == "Player")
        {
            player.enemyCollision = true;
            Debug.Log("ColliderEnnemi-Player");
            //PlayerStats playerStats = collision.transform.GetComponent<PlayerStats>();
            //playerStats.TakeDamage(damageOnCollision);
        }

        if (collision.gameObject.tag == "PlayerSoulColliderEnemy")
        {
            //Debug.Log("PETRIFY");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
