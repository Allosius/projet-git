﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour
{
    public Rigidbody2D target;

    private bool charging;
    private float timeCharged = 0;

    public void ApplyForce(float force, Vector2 direction)
    {
        target.AddForce(direction * force);
    }

    // Start is called before the first frame update
    void Start()
    {
        charging = true;
        timeCharged = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(charging == true)
        {
            timeCharged += Time.deltaTime;
        }
    }
}
