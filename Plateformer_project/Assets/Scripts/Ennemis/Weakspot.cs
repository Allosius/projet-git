﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weakspot : MonoBehaviour
{
    public EnemyStats stats;

    public EnemyPatrol enemyPatrol;

    public GameObject objectToDestroy;

    public SpriteRenderer graphics;

    public GameObject smokeEffect;

    public GameObject gobjPlayer;
    public Player player;

    public float damageFlashDelay = 0.2f;

    public GameObject deathPoint;
    public float deathAnimationTime = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        player = gobjPlayer.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.currentHP <= 0)
        {
            StartCoroutine(Death());
            
        }
    }

    IEnumerator Death()
    {
        enemyPatrol.isDied = true;

        yield return new WaitForSeconds(deathAnimationTime);

        Instantiate(smokeEffect, deathPoint.transform.position, deathPoint.transform.rotation);

        enemyPatrol.isDied = false;
        Destroy(objectToDestroy);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BulletPlayer"))
        {
            //player.enemyCollision = true;
            //stats.currentHP -= 1;
            TakeDamage(1);  
        }
    }

    public void TakeDamage(int damage)
    {
        stats.currentHP -= damage;
        StartCoroutine(EffectDamage());
    }

    public IEnumerator EffectDamage()
    {
        graphics.color = new Color(1f, 0f, 0f, 0.5f);
        yield return new WaitForSeconds(damageFlashDelay);
        graphics.color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(1f);
    }
}
