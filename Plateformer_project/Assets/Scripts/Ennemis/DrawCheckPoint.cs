﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCheckPoint : MonoBehaviour
{
    public GameObject[] checkpoint;

    public float timeToDestroy = 300f;
    public float timeToDestroyMax = 300f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        timeToDestroy -= 1;

        if (timeToDestroy <= 0)
        {
            Debug.Log("checkpoint destroy");
            for (int i = 0; i < checkpoint.Length; i++)
            {
                Destroy(checkpoint[i]);
            }
        }

        if(checkpoint.Length <= 0)
        {
            timeToDestroy = timeToDestroyMax;
        }

        checkpoint = GameObject.FindGameObjectsWithTag("Checkpoint");

        if(checkpoint.Length > 1)
        {
            for(int i = 0; i < checkpoint.Length - 1; i++)
            {
                Debug.DrawLine(checkpoint[i].transform.position, checkpoint[i + 1].transform.position, Color.green);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
