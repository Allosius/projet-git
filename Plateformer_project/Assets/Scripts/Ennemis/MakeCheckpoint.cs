﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeCheckpoint : MonoBehaviour
{
    GameObject objToSpawn;
    public float waitTime = 0.2f;

    public float timeToDestroy = 6f;

    public int damage = 5;

    public GameObject target;
    public float speed;

    public SpriteRenderer graphics;
    public GameObject projectileReturn;
    public GameObject staticProjectile;
    public GameObject staticProjectilePoint;
    public GameObject staticSpear;
    public GameObject playerSoul;

    // Start is called before the first frame update
    void Start()
    {

        staticProjectilePoint = GameObject.FindGameObjectWithTag("StaticSpearPoint");
        staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
        playerSoul = GameObject.FindGameObjectWithTag("PlayerSoul");
        target = GameObject.FindGameObjectWithTag("Player");
    }

    void FixedUpdate()
    {
        timeToDestroy -= 1;

        if (timeToDestroy <= 0)
        {
            Debug.Log("lance destroy");
            Destroy(gameObject);
        }

        objToSpawn = new GameObject("Checkpoint");
        objToSpawn.tag = "Checkpoint";
        objToSpawn.transform.position = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 dir = target.transform.position - transform.position;
        //transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (!PlayerManager.instance.soul)
        {
            if (gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }
        else if (PlayerManager.instance.soul)
        {
            if (gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0.75f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }

        staticProjectilePoint = GameObject.FindGameObjectWithTag("StaticSpearPoint");

        playerSoul = GameObject.FindGameObjectWithTag("PlayerSoul");

        StartCoroutine(DrawPath(waitTime));
    }

    IEnumerator DrawPath(float timeRate)
    {
        while(true)
        {
            objToSpawn = new GameObject("Checkpoint");
            objToSpawn.tag = "Checkpoint";
            objToSpawn.transform.position = this.transform.position;
            yield return new WaitForSeconds(timeRate);
        }
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (PlayerAttack.instance.strike)
        {
            if (hitInfo.CompareTag("ProjectileReturnZone"))
            {
                Instantiate(projectileReturn, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }

        if (hitInfo.CompareTag("Player"))
        {
            Debug.Log("PlayerHitDamages");
            PlayerStats playerStats = hitInfo.transform.GetComponent<PlayerStats>();
            playerStats.TakeDamage(damage);
        }

        if (hitInfo.CompareTag("PlayerSoul"))
        {
            if (gameObject.CompareTag("Spear"))
            {
                PlayerSoul.instance.spear = true;
                staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
                PlayerSoul.instance.arrow = false;
            }

            if (gameObject.CompareTag("Arrow"))
            {
                PlayerSoul.instance.arrow = true;
                staticSpear = GameObject.FindGameObjectWithTag("StaticArrow");
                PlayerSoul.instance.spear = false;
            }
            PlayerSoul.instance.spearStock += 1;
            if (PlayerSoul.instance.spearStock <= 1)
            {
                PlayerSoul.instance.rotationStaticSpear = new Vector3(0f, transform.rotation.y, 0f);
                //Instantiate(staticProjectile, staticProjectilePoint.transform.position, gameObject.transform.rotation);
                SpriteRenderer spriteRenderer = staticSpear.GetComponent<SpriteRenderer>();
                spriteRenderer.enabled = true;
                staticSpear.GetComponent<StaticProjectile>().enabled = true;
                if (PlayerSoul.instance.rotationStaticSpear.y < 0)
                {
                    spriteRenderer.flipX = true;
                }
                else if (PlayerSoul.instance.rotationStaticSpear.y >= 0)
                {
                    spriteRenderer.flipX = false;
                }

                if (playerSoul.transform.rotation.y >= 1)
                {
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                }
                Destroy(gameObject);
            }
        }

        if ((hitInfo.CompareTag("DeathZone")) || (hitInfo.CompareTag("Player")) || (hitInfo.CompareTag("Ground")))
        {
            //Instantiate(impactEffect, transform.position, transform.rotation);
            //AudioManager.instance.PlaySFX(3);

            Destroy(gameObject);
        }

        /*if (hitInfo.CompareTag("Lever"))
        {
            Lever_Spawn_Blocks leverSpawnBlocks = hitInfo.transform.GetComponent<Lever_Spawn_Blocks>();
            leverSpawnBlocks.input = true;

            Debug.Log("projectile_Lever");

            Destroy(gameObject);
        }*/
    }
}
