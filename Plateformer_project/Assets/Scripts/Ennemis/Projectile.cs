﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 50f;
    public int damage = 20;
    public Rigidbody2D rb;
    public SpriteRenderer graphics;
    public GameObject projectileReturn;
    public GameObject staticProjectile;
    public GameObject staticProjectilePoint;
    public GameObject staticSpear;
    public GameObject playerSoul;
    //public GameObject impactEffect;

    // Start is called before the first frame update
    void Start()
    {
        staticProjectilePoint = GameObject.FindGameObjectWithTag("StaticSpearPoint");
        staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
        playerSoul = GameObject.FindGameObjectWithTag("PlayerSoul");
        //gameObject.transform.Rotate(0.0f, 0.0f, 90.0f, Space.Self);
        rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerManager.instance.soul)
        {
            if (gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }
        else if (PlayerManager.instance.soul)
        {
            if (gameObject.layer == 17)
            {
                graphics.color = new Color(0f, 1f, 1f, 0.75f);
            }
            else
            {
                graphics.color = new Color(1f, 1f, 1f, 1f);
            }
        }

        //Debug.Log(transform.rotation.y);
        //Debug.Log(transform.localRotation.y);
        staticProjectilePoint = GameObject.FindGameObjectWithTag("StaticSpearPoint");
        //staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
        playerSoul = GameObject.FindGameObjectWithTag("PlayerSoul");
        //Debug.Log("strike :" + PlayerAttack.instance.strike);
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (PlayerAttack.instance.strike)
        {
            if (hitInfo.CompareTag("ProjectileReturnZone"))
            {
                Instantiate(projectileReturn, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }

        if (hitInfo.CompareTag("Player"))
        {
            Debug.Log("PlayerHitDamages");
            PlayerStats playerStats = hitInfo.transform.GetComponent<PlayerStats>();
            playerStats.TakeDamage(damage);
        }

        if (hitInfo.CompareTag("PlayerSoul"))
        {
            if(gameObject.CompareTag("Spear"))
            {
                PlayerSoul.instance.spear = true;
                staticSpear = GameObject.FindGameObjectWithTag("StaticSpear");
                PlayerSoul.instance.arrow = false;
            }

            if(gameObject.CompareTag("Arrow"))
            {
                PlayerSoul.instance.arrow = true;
                staticSpear = GameObject.FindGameObjectWithTag("StaticArrow");
                PlayerSoul.instance.spear = false;
            }
            PlayerSoul.instance.spearStock += 1;
            if (PlayerSoul.instance.spearStock <= 1)
            {
                PlayerSoul.instance.rotationStaticSpear = new Vector3(0f, transform.rotation.y, 0f);
                //Instantiate(staticProjectile, staticProjectilePoint.transform.position, gameObject.transform.rotation);
                SpriteRenderer spriteRenderer = staticSpear.GetComponent<SpriteRenderer>();
                spriteRenderer.enabled = true;
                staticSpear.GetComponent<StaticProjectile>().enabled = true;
                if (PlayerSoul.instance.rotationStaticSpear.y < 0)
                {
                    spriteRenderer.flipX = true;
                }
                else if (PlayerSoul.instance.rotationStaticSpear.y >= 0)
                {
                    spriteRenderer.flipX = false;
                }

                if (playerSoul.transform.rotation.y >= 1)
                {
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                }
                Destroy(gameObject);
            }
        }

        if ((hitInfo.CompareTag("DeathZone")) || (hitInfo.CompareTag("Player")) || (hitInfo.CompareTag("Ground")))
        {
            //Instantiate(impactEffect, transform.position, transform.rotation);
            //AudioManager.instance.PlaySFX(3);

            Destroy(gameObject);
        }

        /*if(hitInfo.CompareTag("Lever"))
        {
            Lever_Spawn_Blocks leverSpawnBlocks = hitInfo.transform.GetComponent<Lever_Spawn_Blocks>();
            leverSpawnBlocks.input = true;

            Debug.Log("projectile_Lever");

            Destroy(gameObject);
        }*/
    }
}
