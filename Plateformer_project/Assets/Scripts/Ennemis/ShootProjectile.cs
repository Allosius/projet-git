﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootProjectile : MonoBehaviour
{
    public EnemyPatrol enemyPatrol;

    public Transform firePoint;
    public GameObject bulletPrefab;
    public float attackAnimationTime = 0.5f;
    public bool isFiring = true;
    public float countdown = 7.5f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isFiring == true)
        {
            StartCoroutine(Shoot());
            isFiring = false;
        }
    }

    IEnumerator Shoot()
    {
        enemyPatrol.isAttacking = true;
        yield return new WaitForSeconds(attackAnimationTime);
        // shooting logic
        Instantiate(bulletPrefab, firePoint.position, transform.rotation);
        enemyPatrol.isAttacking = false;
        //AudioManager.instance.PlaySFX(4);
        yield return new WaitForSeconds(countdown);
        isFiring = true;
    }

    /*void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.CompareTag("BulletPlayer"))
        {
            //Instantiate(impactEffect, transform.position, transform.rotation);
            //AudioManager.instance.PlaySFX(3);

            Destroy(gameObject);
        }
    }*/
}
