﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSpears : MonoBehaviour
{


    public Transform target;
    public Transform throwPoint;
    public Transform playerPoint;
    public GameObject ball;
    public float timeTillHit = 1f;

    public EnemyPatrol enemyPatrol;

    public float attackAnimationTime = 0.5f;
    public bool isFiring = true;
    public float countdown = 7.5f;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    void Throw()
    {
        float xdistance;
        xdistance = playerPoint.position.x - throwPoint.position.x;

        float ydistance;
        ydistance = target.position.y - throwPoint.position.y;

        float throwAngle; // in radian

        throwAngle = Mathf.Atan((ydistance + 2.5f*(timeTillHit*timeTillHit)) / xdistance);

        float totalVelo = xdistance / (Mathf.Cos(throwAngle) * timeTillHit);

        float xVelo, yVelo;
        xVelo = totalVelo * Mathf.Cos(throwAngle);
        yVelo = totalVelo * Mathf.Sin(throwAngle);

        GameObject bulletInstance = Instantiate(ball, throwPoint.position, transform.rotation) as GameObject;
        Rigidbody2D rigid;
        rigid = bulletInstance.GetComponent<Rigidbody2D>();
        rigid.velocity = new Vector2(xVelo, yVelo);
    }

    IEnumerator Shoot()
    {
        enemyPatrol.isAttacking = true;
        yield return new WaitForSeconds(attackAnimationTime);
        // shooting logic
        Throw();
        enemyPatrol.isAttacking = false;
        //AudioManager.instance.PlaySFX(4);
        yield return new WaitForSeconds(countdown);
        isFiring = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (isFiring == true)
        {
            StartCoroutine(Shoot());
            isFiring = false;
        }
    }
}
