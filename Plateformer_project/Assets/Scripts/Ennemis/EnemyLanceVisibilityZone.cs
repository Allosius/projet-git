﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLanceVisibilityZone : MonoBehaviour
{
    public ShootSpears shootSpears;
    public GameObject graphics;

    // Start is called before the first frame update
    void Start()
    {
        shootSpears.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = graphics.transform.position;
        transform.rotation = graphics.transform.rotation;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!PlayerManager.instance.soul)
        {
            if (graphics.layer == 17)
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootSpears.enabled = false;
                }
            }
            else
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootSpears.enabled = true;
                }
            }
        }
        else if (PlayerManager.instance.soul)
        {
            if (graphics.layer == 17)
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootSpears.enabled = true;
                }
            }
            else
            {
                if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
                {
                    shootSpears.enabled = false;
                }
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
        {
            shootSpears.enabled = false;
        }
    }
}
