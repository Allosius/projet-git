﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationManager : MonoBehaviour
{
    public GridFunction gridFunction;

    private float gridMoveSpeed;

    public float translationTime;

    public bool up;

    public bool down;

    

    // Start is called before the first frame update
    void Start()
    {
        gridMoveSpeed = gridFunction.moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if(up)
        {
            StartCoroutine(GridUp());
        }
        else if (down)
        {
            StartCoroutine(GridDown());
        }
        
    }

    private IEnumerator GridUp()
    {
        up = false;
        gridFunction.up = true;
        gridFunction.moveSpeed = gridMoveSpeed;
        yield return new WaitForSeconds(translationTime);
        down = true;
    }

    private IEnumerator GridDown()
    {
        down = false;
        gridFunction.up = false;
        gridFunction.moveSpeed = gridMoveSpeed;
        yield return new WaitForSeconds(translationTime);
        up = true;
    }
}
