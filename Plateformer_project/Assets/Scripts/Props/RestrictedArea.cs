﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestrictedArea : MonoBehaviour
{
    /*public float speed = 5f;

    private Vector3 velocity = Vector3.zero;
    Vector3 targetVelocity;
    private float movement;

    public float thrust = 10.0f;*/

    //public Player player;

    public Transform playerSoul;
    public Transform respawnPointLeft;
    public Transform respawnPointRight;
    private float timer;

    public float knockbackDistance = 0.2f;
    public float knockbackHeight = 0.1f;
    public int knockbackDuration = 10;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //movement = speed * Time.fixedDeltaTime;

        if (PlayerManager.instance.gridDeath && PlayerManager.instance.soul)
        {
            if ((Vector3.Distance(playerSoul.position, respawnPointRight.position)) <= (Vector3.Distance(playerSoul.position, respawnPointLeft.position)))
            {
                playerSoul.position = respawnPointRight.position;
                PlayerManager.instance.gridDeath = false;
            }
            else if ((Vector3.Distance(playerSoul.position, respawnPointRight.position)) > (Vector3.Distance(playerSoul.position, respawnPointLeft.position)))
            {
                playerSoul.position = respawnPointLeft.position;
                PlayerManager.instance.gridDeath = false;
            }
        }
        else
        {
            if (timer != 0)
            {
                if (playerSoul.position.x > transform.position.x)
                {
                    playerSoul.position += new Vector3(knockbackDistance, knockbackHeight, 0f);
                    timer -= 1;
                }
                else if (playerSoul.position.x < transform.position.x)
                {
                    playerSoul.position -= new Vector3(knockbackDistance, knockbackHeight, 0f);
                    timer -= 1;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            PlayerManager.instance.restrictedArea = true;
        }

        if (collision.CompareTag("PlayerSoul"))
        {
            //player = collision.transform.position;

            timer = knockbackDuration;

            //StartCoroutine(Knockback());

            //StartCoroutine(player.Knockback(20f, 3500, player.transform.position));
            //Debug.Log("Knockback Player Soul");

            //collision.attachedRigidbody.AddForce(collision.transform.right * thrust);

            //targetVelocity = new Vector2(movement, collision.attachedRigidbody.velocity.y);
            //collision.attachedRigidbody.velocity = Vector3.SmoothDamp(collision.attachedRigidbody.velocity, targetVelocity, ref velocity, 0.05f);
            //collision.transform.position = new Vector3(collision.transform.position.x + 0.75f, collision.transform.position.y, collision.transform.position.z);
            //collision.transform.Translate(new Vector3(collision.transform.position.x + 0.75f, collision.transform.position.y, collision.transform.position.z));
            //collision.transform.position += new Vector3(1f, 0f, 0f);
        }
    }

    /*public IEnumerator Knockback()
    {
        yield return new WaitForSeconds(0.01f);

        timer = knockbackDuration;
    }*/

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerManager.instance.restrictedArea = false;
        }
    }
}


