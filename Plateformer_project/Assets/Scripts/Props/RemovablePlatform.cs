﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemovablePlatform : MonoBehaviour
{
    public SpriteRenderer[] spriteRenderer;
    public Sprite spriteControl;
    public Sprite spriteUncontrol;

    public Transform pos1, pos2;
    public float speed;
    public Transform startPos;

    //public Vector3 nextPos;
    public bool down = true;
    public bool up = true;
    public bool soulControlling;
    public bool input;
    public bool stopControl;

    public GameObject trigger;
    public GameObject playerSoul;
    public Transform transformPlayerSoul;

    // Start is called before the first frame update
    void Start()
    {
        //nextPos = startPos.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position == pos1.position)
        {
            down = false;
        }
        else
        {
            down = true;
        }
        if (transform.position == pos2.position)
        {
            up = false;
        }
        else
        {
            up = true;
        }

        if ((input) && (!soulControlling))
        {
            PlayerManager.instance.notControllingPlateform = false;

            CameraFollow.instance.soul = gameObject;

            playerSoul.SetActive(false);
            soulControlling = true;
            trigger.SetActive(false);
            StartCoroutine(StopControl());
        }

        if (Input.GetButtonDown("Soul"))
        {
            if (soulControlling && stopControl)
            {
                trigger.SetActive(true);
                stopControl = false;

                PlayerManager.instance.notControllingPlateform = true;

                CameraFollow.instance.soul = playerSoul;

                transformPlayerSoul.position = new Vector3(transform.position.x, transform.position.y + 0.573f, transform.position.z);
                playerSoul.SetActive(true);

                PlayerManager.instance.BodyForm();

                soulControlling = false;
            }
        }

        if (soulControlling)
        {
            for (int i = 0; i < spriteRenderer.Length; i++)
            {
                spriteRenderer[i].sprite = spriteControl;
            }

            if ((Input.GetButton("Down_Platform")) && (down == true))
            {
                transform.position = Vector3.MoveTowards(transform.position, pos1.position, speed * Time.deltaTime);
            }
            else if ((Input.GetButton("Up_Platform")) && (up == true))
            {
                transform.position = Vector3.MoveTowards(transform.position, pos2.position, speed * Time.deltaTime);
            }
        }
        else
        {
            for (int i = 0; i < spriteRenderer.Length; i++)
            {
                spriteRenderer[i].sprite = spriteUncontrol;
            }
        }
        


        //transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerSoul"))
        {
            input = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerSoul"))
        {
            input = false;
        }
    }

    public IEnumerator StopControl()
    {
        yield return new WaitForSeconds(0.5f);
        stopControl = true;
    }

    /*public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "PlayerSoul")
        {
            input = true;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "PlayerSoul")
        {
            input = false;
        }
    }*/
}   
