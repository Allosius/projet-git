﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownWallLever : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite spriteButtonOff;
    public Sprite spriteButtonOn;
    private float wallMoveSpeed;
    public bool input;
    public bool active;

    public DownWall downWall;

    // Start is called before the first frame update
    void Start()
    {
        active = false;

        spriteRenderer.sprite = spriteButtonOff;

        wallMoveSpeed = downWall.moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
        {
            if ((Input.GetMouseButtonDown(1)) && (input))
            {
                spriteRenderer.sprite = spriteButtonOn;
                downWall.down = true;
                downWall.moveSpeed = wallMoveSpeed;
                active = true;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (PlayerManager.instance.soul)
        {
            if (collision.CompareTag("PlayerSoul"))
            {
                input = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerSoul"))
        {
            input = false;
        }
    }
}
