﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulCrate : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed = 5f;
    public float rightSpeed = 5f;
    public float leftSpeed = 5f;
    private float temp = 100f;
    public bool contact;

    public bool isGrounded;

    public Player player;

    private Vector3 velocity = Vector3.zero;
    Vector3 targetVelocity;
    private float movement;

    // Start is called before the first frame update
    void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        Debug.Log(transform.rotation);
        /*if (transform.rotation.y == 1)
        {
            temp = leftSpeed;
            leftSpeed = rightSpeed;
            rightSpeed = temp;
        }*/


    }

    void FixedUpdate()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerManager.instance.soul)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;

            //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player = PlayerManager.instance.playerSoul.GetComponent<Player>();
            //Debug.Log("Crate- player");
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;

            //player = GameObject.FindGameObjectWithTag("PlayerSoul").GetComponent<Player>();
            player = PlayerManager.instance.playerBody.GetComponent<Player>();
            //Debug.Log("Crate-player soul");
        }

        movement = speed * Time.fixedDeltaTime;
        targetVelocity = new Vector2(movement, rb.velocity.y);

        if (PlayerManager.instance.playerIsFlipping)
        {
            speed = leftSpeed;
        }
        else
        {
            speed = rightSpeed;
        }

        if (PlayerManager.instance.soul)
        {
            if ((PlayerSoulPush.instance.push) && (contact) && (player.controller.isMoving))
            {
                rb.constraints = RigidbodyConstraints2D.None;
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;

                rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);
                //rb.velocity = transform.right * speed;
                player.isPushing = true;
            }

            else
            {
                if (isGrounded)
                {
                    rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
                }
                else
                {
                    rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                    //player.isPushing = false;
                    //StartCoroutine(PushingOff());
                }
            }

            if (!PlayerSoulPush.instance.push)
            {
                //Debug.Log("Exit-Crate");
                player.isPushing = false;
                //rb.constraints = RigidbodyConstraints2D.FreezeAll;
            }
        }
    }

    /*IEnumerator PushingOff()
    {
        yield return new WaitForSeconds(1f);
        player.isPushing = false;
    }*/

    void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.tag == "Ground"))
        {
            Debug.Log("Crate-Ground");
            isGrounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Debug.Log("Crate-Air");
            isGrounded = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        /*if (collision.CompareTag("Player"))
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }*/
    }
}
