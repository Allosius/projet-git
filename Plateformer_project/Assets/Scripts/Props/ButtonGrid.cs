﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonGrid : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite spriteButtonOff;
    public Sprite spriteButtonOn;
    private float gridMoveSpeed;

    public GridFunction gridFunction;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer.sprite = spriteButtonOff;

        gridMoveSpeed = gridFunction.moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
        {
            gridFunction.up = true;
            gridFunction.moveSpeed = gridMoveSpeed;
            spriteRenderer.sprite = spriteButtonOn;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.CompareTag("Player")) || (collision.CompareTag("PlayerSoul")))
        {
            StartCoroutine(GridClose());
            spriteRenderer.sprite = spriteButtonOff;
        }
    }

    private IEnumerator GridClose()
    {
        yield return new WaitForSeconds(2.5f);
        gridFunction.up = false;
        gridFunction.moveSpeed = gridMoveSpeed;
    }
}
