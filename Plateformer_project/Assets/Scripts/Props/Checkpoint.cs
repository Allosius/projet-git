﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Transform playerSpawn;
    public GameObject vfxBlueFire;

    private void Awake()
    {
        playerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerSpawn.position = transform.position;
            vfxBlueFire.SetActive(true);    // active l'animation de flammes après avoir passé/activé le checkpoint
            AudioManager.instance.PlaySFX(5);
            gameObject.GetComponent<BoxCollider2D>().enabled = false; // permet de desactiver le collider du checkpoint une fois passé, mais sans supprimer le visuel
        }
    }
}
