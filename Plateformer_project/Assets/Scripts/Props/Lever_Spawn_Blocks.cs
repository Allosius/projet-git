﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever_Spawn_Blocks : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite spriteButtonOff;
    public Sprite spriteButtonOn;
    public bool active;
    public bool input;

    public GameObject[] spawnBlocks;

    // Start is called before the first frame update
    void Start()
    {
        active = false;

        spriteRenderer.sprite = spriteButtonOff;
    }

    // Update is called once per frame
    void Update()
    {
        if(input == true && !active)
        {
            spriteRenderer.sprite = spriteButtonOn;

            for (int i = 0; i < spawnBlocks.Length; i++)
            {
                spawnBlocks[i].SetActive(true);
            }
            
            active = true;
            input = false;
        }
    }
}
