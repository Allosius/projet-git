﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    //public Transform playerSpawn;
    //public Animator fadeSystem;
    //public GameObject player;

    private void Awake()
    {
        //playerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
        //fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartCoroutine(Defeat(/*collision*/));
        }
    }

    private IEnumerator Defeat(/*Collider2D collision*/)
    {
        //fadeSystem.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1f);
        //collision.transform.position = playerSpawn.position;
        //player.transform.position = playerSpawn.position;
        //AudioManager.instance.PlaySFX(10);
        GameManager.instance.OnPlayerDeath();
    }
}
