﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulLeverSpawnBlocks : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite spriteButtonOff;
    public Sprite spriteButtonOn;
    public bool active;
    public bool input;

    public GameObject[] spawnBlocks;

    // Start is called before the first frame update
    void Start()
    {
        active = false;

        spriteRenderer.sprite = spriteButtonOff;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerManager.instance.soul)
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (input == true && !active)
        {
            spriteRenderer.sprite = spriteButtonOn;

            for (int i = 0; i < spawnBlocks.Length; i++)
            {
                spawnBlocks[i].SetActive(true);
            }

            active = true;
            input = false;
        }

        if (active)
        {
            if (PlayerManager.instance.soul)
            {
                for (int i = 0; i < spawnBlocks.Length; i++)
                {
                    spawnBlocks[i].GetComponent<SpriteRenderer>().enabled = true;
                }
            }
            else
            {
                for (int i = 0; i < spawnBlocks.Length; i++)
                {
                    spawnBlocks[i].GetComponent<SpriteRenderer>().enabled = true;
                }
            }
        }
    }
}
