﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform transformPlayerStatic;
    public Transform transformPlayerSoul;

    public bool input;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetMouseButtonDown(1)) && (input))
        {
            Debug.Log("Teleporting");
            transformPlayerStatic.position = transformPlayerSoul.position;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (PlayerManager.instance.soul)
        {
            if (collision.CompareTag("PlayerSoul"))
            {
                input = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerSoul"))
        {
            input = false;
        }
    }
}
