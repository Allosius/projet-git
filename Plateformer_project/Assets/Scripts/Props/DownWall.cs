﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownWall : MonoBehaviour
{
    public bool down;

    public float moveSpeed;

    public Transform target;
    public Rigidbody2D rb;

    private Vector3 velocity = Vector3.zero;
    private float verticalMovementDown;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (down)
        {
            Vector3 targetVelocity = new Vector2(rb.velocity.x, verticalMovementDown);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);

            if (Vector3.Distance(transform.position, target.position) < 0.3f)
            {
                moveSpeed = 0f;
            }
        }
    }

    void FixedUpdate()
    {
        verticalMovementDown = -moveSpeed * Time.fixedDeltaTime;
    }
}
