﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridFunction : MonoBehaviour
{
    public bool up;

    public float moveSpeed;

    public float pauseTime = 1.5f;

    public Transform targetUp;
    public Transform targetDown;
    public Rigidbody2D rb;

    public Rigidbody2D rbInvisibleCollider;
    //public Collider2D colliderInvisibleCollider;
    public bool invisibleCollider;

    private Vector3 velocity = Vector3.zero;
    private float verticalMovementUp;
    private float verticalMovementDown;

    /*public static GridFunction instance;

    private void Awake()
    {
        if (instance != null)
        {
            return;
        }

        instance = this;
    }*/

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        verticalMovementUp = moveSpeed * Time.fixedDeltaTime;

        verticalMovementDown = -moveSpeed * Time.fixedDeltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (up)
        {
            Vector3 targetVelocity = new Vector2(rb.velocity.x, verticalMovementUp);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);

            //rbInvisibleCollider.bodyType = RigidbodyType2D.Kinematic;
            invisibleCollider = false;

            if (Vector3.Distance(transform.position, targetUp.position) < 0.3f)
            {
                moveSpeed = 0f;
            }
        }
        else if (!up)
        {
            Vector3 targetVelocity = new Vector2(rb.velocity.x, verticalMovementDown);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.05f);

            invisibleCollider = true;

            if (Vector3.Distance(transform.position, targetDown.position) < 0.3f)
            {
                moveSpeed = 0f;
                
                //rbInvisibleCollider.bodyType = RigidbodyType2D.Kinematic;
            }
        }
    }
}
