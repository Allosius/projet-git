﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCollider : MonoBehaviour
{
    public GridFunction gridFunction;
    public PlayerStats playerStats;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("grid death :" + PlayerManager.instance.gridDeath);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerWeakspot"))
        {
            if(gridFunction.invisibleCollider)
            {
                PlayerManager.instance.gridDeath = true;
                Debug.Log("Player Death");
                //GameManager.instance.OnPlayerDeath();
                playerStats.TakeDamage(1);
                
            }
        }
    }
}
