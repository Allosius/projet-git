﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeEffect : MonoBehaviour
{
    public float duration;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TimeBeforeDestroy());
    }

    // Update is called once per frame
    private IEnumerator TimeBeforeDestroy()
    {
        yield return new WaitForSeconds(duration);
        Destroy(gameObject);
    }
}
