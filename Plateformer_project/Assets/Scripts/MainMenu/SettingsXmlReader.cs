﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SettingsXmlReader : MonoBehaviour
{
    public TextAsset dictionary;

    public string languageName;
    public int currentLanguage;

    string bonjour;
    string aurevoir;
    string options;
    string pleinecran;
    //string qualitegraphique;
    string volume;

    public Text textBonjour;
    public Text textAurevoir;
    public Text textOptions;
    public Text textPleinecran;
    //public Text textQualitegraphique;
    public Text textVolume;
    public Dropdown selectDropdown;

    List<Dictionary<string, string>> languages = new List<Dictionary<string, string>>();
    Dictionary<string, string> obj;

    // Start is called before the first frame update
    void Awake()
    {
        Reader();
    }

    // Update is called once per frame
    void Update()
    {
        languages[currentLanguage].TryGetValue("Name", out languageName);
        languages[currentLanguage].TryGetValue("bonjour", out bonjour);
        languages[currentLanguage].TryGetValue("aurevoir", out aurevoir);
        languages[currentLanguage].TryGetValue("options", out options);
        languages[currentLanguage].TryGetValue("pleinecran", out pleinecran);
        //languages[currentLanguage].TryGetValue("qualitegraphique", out qualitegraphique);
        languages[currentLanguage].TryGetValue("volume", out volume);

        textBonjour.text = bonjour;
        textAurevoir.text = aurevoir;
        textOptions.text = options;
        textPleinecran.text = pleinecran;
        //textQualitegraphique.text = qualitegraphique;
        textVolume.text = volume;
    }

    void Reader()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(dictionary.text);
        XmlNodeList languageList = xmlDoc.GetElementsByTagName("language");

        foreach (XmlNode languageValue in languageList)
        {
            XmlNodeList languageContent = languageValue.ChildNodes;
            obj = new Dictionary<string, string>();

            foreach (XmlNode value in languageContent)
            {
                if (value.Name == "Name")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "bonjour")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "aurevoir")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "options")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "pleinecran")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                /*if (value.Name == "qualitegraphique")
                {
                    obj.Add(value.Name, value.InnerText);
                }*/

                if (value.Name == "volume")
                {
                    obj.Add(value.Name, value.InnerText);
                }
            }

            languages.Add(obj);
        }
    }

    public void ValueChangeCheck()
    {
        currentLanguage = selectDropdown.value;
    }
}
