﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //public int musicIndex;

    public string GameOverScene;
    public string currentScene;

    public SpriteRenderer[] spriteRendererSoulPlatform;

    public static GameManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de GameManager dans la scène");
            return;
        }

        instance = this;
    }

    public void OnPlayerDeath()
    {
        SceneManager.LoadScene(GameOverScene);
    }

    // Start is called before the first frame update
    void Start()
    {
        //AudioManager.instance.StopMusic();
        //AudioManager.instance.PlayBGM(musicIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Retry"))
        {
            SceneManager.LoadScene(currentScene);
        }

        if(PlayerManager.instance.soul)
        {
            for (int i = 0; i < spriteRendererSoulPlatform.Length; i++)
            {
                spriteRendererSoulPlatform[i].enabled = true;
            }
        }
        else
        {
            for (int i = 0; i < spriteRendererSoulPlatform.Length; i++)
            {
                spriteRendererSoulPlatform[i].enabled = false;
            }
        }
    }
}
