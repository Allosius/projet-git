﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public GameObject body;
    public GameObject soul;

    public float timeOffset;
    public Vector3 posOffset;

    public float xpos;
    public float xneg;

    [SerializeField] private Transform bodyTransform = null;
    [SerializeField] private Transform soulTransform = null;
    [SerializeField] private Transform followTransform = null;
    [SerializeField] private float followZOffset = -1;
    [SerializeField] private float widthSafeArea = 10.0f;
    [SerializeField] private float heightSafeArea = 5.0f;
    [SerializeField] private float cameraSpeed = 10.0f;
    [SerializeField] private Vector3 velocity;
    [SerializeField] private float drag = 10.0f;

    public static CameraFollow instance;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de CameraFollow dans la scène");
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        xpos = posOffset.x;
        xneg = -posOffset.x;
    }

    private void FixedUpdate()
    {
        /*if (PlayerManager.instance.soul)
        {
            followTransform = soulTransform;
        }
        else if (!PlayerManager.instance.soul)
        {
            followTransform = bodyTransform;
        }*/

        /*if (followTransform)
        {
            Vector3 followP = followTransform.position;
            Vector3 oldP = transform.position;

            float differenceInX = Mathf.Abs((followP.x - oldP.x));
            float differenceInY = Mathf.Abs((followP.y - oldP.y));

            Vector3 direction = Vector3.zero;
            if(differenceInX >= widthSafeArea || differenceInY >= heightSafeArea)
            {
                direction = (followP - oldP).normalized;
            }

            float speed = cameraSpeed;
            Vector3 acceleration = direction * speed;
            acceleration += -drag * velocity;
            Vector3 newP = 0.5f * acceleration * (Time.deltaTime * Time.deltaTime) + velocity * Time.deltaTime + oldP;
            velocity = acceleration * Time.deltaTime + velocity;
            newP.z = followP.z + followZOffset;
            transform.position = newP;
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerManager.instance.soul)
        {
            player = soul;
        }
        else if (!PlayerManager.instance.soul)
        {
            player = body;
        }

        transform.position = Vector3.SmoothDamp(transform.position, player.transform.position + posOffset, ref velocity, timeOffset);
    }
}
