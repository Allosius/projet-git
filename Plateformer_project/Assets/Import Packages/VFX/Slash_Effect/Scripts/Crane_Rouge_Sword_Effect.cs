﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StateAnim02
{
    Idle,
    Move
}
public class Crane_Rouge_Sword_Effect : MonoBehaviour
{

    public Animator anim;

    public GameObject SlashEffect;

    public float AnimLength = 0f;
    public float LaunchSlashLength = 0f;

    public bool Trail = false;
    public bool Slash = false;

    private StateAnim02 state;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (AnimLength <= 0 && Trail == false)
        {
            if (Input.GetButton("Jump"))
            {
                Trail = true;
                AnimLength = 119f;
                Slash = true;
                LaunchSlashLength = 60f;
                anim.Play("CraneRougeSwordTrailAnimation");

            }
        }

        if (Slash == true)
        {
            LaunchSlashLength--;
        }

        if (LaunchSlashLength <= 0)
        {
            Slash = false;
            SlashEffect.SetActive(true);
        }

        if (Trail == true)
        {
            AnimLength--;
        }

        if (AnimLength <= 0)
        {
            Trail = false;
            anim.Play("CraneRougeSwordIdleAnimation");
            SlashEffect.SetActive(false);
        }

        Debug.Log(AnimLength);
    }
}
