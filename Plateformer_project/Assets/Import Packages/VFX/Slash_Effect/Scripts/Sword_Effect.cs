﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StateAnim
{
    Idle,
    Move
}
public class Sword_Effect : MonoBehaviour
{

    public Animator anim;

    public GameObject SlashEffect;

    public float AnimLength = 0f;
    public float LaunchSlashLength = 0f;

    public bool Trail = false;
    public bool Slash = false;

    private StateAnim state;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(AnimLength <= 0 && Trail == false)
        {
            if (Input.GetButton("Jump"))
            {
                Trail = true;
                AnimLength = 119f;
                Slash = true;
                LaunchSlashLength = 75f;
                anim.Play("MagicSwordTrailAnimation");
                
            }
        }

        if(Slash == true)
        {
            LaunchSlashLength--;
        }

        if(LaunchSlashLength <= 0)
        {
            Slash = false;
            SlashEffect.SetActive(true);
        }

        if(Trail == true)
        {
            AnimLength--;
        }

        if(AnimLength <= 0)
        {
            Trail = false;
            anim.Play("IdleAnimation");
            SlashEffect.SetActive(false);
        }

        Debug.Log(AnimLength);
    }
}
