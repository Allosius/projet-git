﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public float Speed = 2f;
    public Ghost ghost;

    // Start is called before the first frame update
    void Start()
    {
        ghost.makeGhost = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            ghost.makeGhost = true;
            float newX = transform.position.x + Speed * Time.deltaTime;
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            ghost.makeGhost = true;
            float newX = transform.position.x - Speed * Time.deltaTime;
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

        else if (Input.GetKey(KeyCode.DownArrow))
        {
            ghost.makeGhost = true;
            float newY = transform.position.y + Speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        }

        else if (Input.GetKey(KeyCode.UpArrow))
        {
            ghost.makeGhost = true;
            float newY = transform.position.y - Speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        }

        else
        {
            ghost.makeGhost = false;
        }
    }
}
